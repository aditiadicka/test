@extends('layouts.app')


@section('content')

    <form action="{{ route('user.update', $user->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        <div class="form-group">
            <label for="name">User Name:</label>
            <input type="text" class="form-control" name="name" value="{{ $user->name }}" id="name">
        </div>
        <div class="form-group">
            <label for="email">Email address:</label>
            <input type="email" class="form-control" name="email" value="{{ $user->email}}" id="email">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" name="password" value="{{ $user->password }}" id="password">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
@endsection