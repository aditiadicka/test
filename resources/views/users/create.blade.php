@extends('layouts.app')


@section('content')

    <form action="{{ route('user.store') }}" method="post">
    {{ csrf_field() }}
        <div class="form-group">
            <label for="name">User Name:</label>
            <input type="text" class="form-control" name="name" id="name">
        </div>
      <div class="form-group">
        <label for="email">Email address:</label>
        <input type="email" class="form-control" name="email" id="email">
      </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" name="password" id="password">
        </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
@endsection