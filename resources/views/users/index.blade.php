@extends('layouts.app')


@section('content')

    <table class="table">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
                        <a href="{{ route('user.destroy', $user->id) }}" class="btn btn-danger">Destroy</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
    </table>

@endsection